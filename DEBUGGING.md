# The portal

## Kubectl

You can get all of the services (assuming that you set your kuneconfig) with

```
kubectl get pods -n $NAMESPACE
```

where `$NAMESPACE` is one of the following:

- `merge`
- `merge-auth`
- `xdc`

Most of the main ones are in `merge`.

To view the log of a pod, use

```
./collider/portal/util/logs.sh $NAMESPACE $POD
```

as an example:

```
./collider/portal/util/logs.sh merge apiserver
```

# The Facility

## SSH

When you place the keys with

```
./collider/facility/place-keys.sh
```

You can SSH into the various machines with:

```
ssh -F phobos/phobos-ssh.config $MACHINE
```

where `$MACHINE` is one of the following:

- `ops`
- `ifr`
- `xp`
- `infra`
- `x0`
- `x1`
- `x2`
- `x3`

## Checking Harbor Status

```
ssh -F phobos/phobos-ssh.config ops
mrs show mz harbor.system.marstb -S
```

If you don't have a harbor status, then either your version was before the status merge or that the harbor initialization failed.
If it failed, you can try this to reinitialize the harbor:

```
ssh -F phobos/phobos-ssh.config ops
mrs deinit harbor ; sleep 10 ; mrs init harbor
```

If this fails, you'll want to check the logs from the `apiserver` on `ifr`, instructions are below.

## Checking services generally

You'll want to make sure that everything in the harbor status is a success.
If something is not a success, you can SSH into one of the machines and check the logs of the reconciler.
Most of the logs will be on the `ifr` server and there are two types of services:

- `ifr` level services
- services specific to a specific infrapod

For the `ifr` level services, you can check them with

```
ssh -F phobos/phobos-ssh.config ifr
sudo journalctl -u mars-$SERVICE
```

Where `$SERVICE` is one of the following:

- `apiserver`
- `canopy`
- `cniman`
- `etcd`
- `frr`
- `infrapod`
- `install`
- `metal`
- `minio`
- `wireguard`

Generally, you'll want to check the service that's failed in your harbor status.

If you need to, you can update/kick the service by running `sudo systemctl restart` with the full name you used above.

However, if your taskkey isn't one of theose and just has `harbor.system.marstb` within it,
that means that service is running as a `podman` "infrapod" service, so you'll to use:

```
ssh -F phobos/phobos-ssh.config ifr

# List all of the pods

sudo podman ps

# Pick something from the NAMES column and get the logs

sudo podman logs $NAME
```

These ones are harder to kick/update, you'll have to kick the entire pod with:

```
ssh -F phobos/phobos-ssh.config ifr

# List pods
sudo podman pod ps

# Kick the entire pod, pick a name from the NAME column
sudo podman pod rm -f $POD
```

You'll then either have to wait for `mars-infrapod` to bring it back up (checks every minute), or you can manually kick it with `sudo systemctl restart mars-infrapod` to immediately bring it back up.

## Checking sled

You'll want to check this if it seems like your nodes are failing to image.
These corespond to the `metalCreateOps` sub-category.
You can also view these with:

```
ssh -F phobos/phobos-ssh.config ops
mrs list metal
```

You can also view the sled logs with:

```
ssh -F phobos/phobos-ssh.config ifr
sudo podman logs harbor.system.marstb-sled
```

If it's stuck on `waiting to connect...`, you can manually watch the screens of the machines with

```
# from the host machine
sudo virt-manager
```

and opening the machines in the GUI.

If you do not want to use sudo to open `virt-manager` (particularly useful for X11 forwarding reasons), you can follow these instructions to allow yourself access without root:
https://computingforgeeks.com/use-virt-manager-as-non-root-user/

You'll want to see that they're PXE booting into sled:

![](collider/images/0-pxe.jpg)

Get into sled:

![](collider/images/1-sled.jpg)

(You can view the imaging status with `mrs list metal` or the serial console.)

Get into the image:

![](collider/images/2-image.jpg)

If you want to manually reset the Sled status to forcefully **reboot** and **re-image** a machine, you can use:

```
ssh -F phobos/phobos-ssh.config ops

# This will reset an individual machine
mrs reset key -do /metal/$MACHINE

# This command will reset all machines:
mrs reset key -dop /metal/
```

This only works for machines listed in `mrs list metal`, so no emulation servers and the like.

## Checking networking

You can check this either via the specific `mtz` status keys (the `linkCreateOps` group) or the entire facility with `mrs list net` from `ops`.

Networking is done on switches and most machines via the canopy reconciler/service.
Checking the logs are slightly different from the switches and the machines.

On the switches (`infra`, `xp`), you can SSH into them and use

```
ssh -F phobos/phobos-ssh.config $SWITCH
sudo journalctl -u canopy@mgmt.service
```

On other machines (`ifr`, `x*`, `emu`), you use

```
ssh -F phobos/phobos-ssh.config $MACHINE
sudo journalctl -u mars-canopy.service
```

## Wireguard

The portal needs a route to the `ifr` name specifically for wireguard connections to work.
If you can ping `ping -c 1 ifr` from your host machine, you're probably ok?
If you don't, you'll need to set up a route to `ifr` on your host machine.

## Harbor Materialization Path

If something higher in the path fails, you probably will not see anything below.

- `mars-apiserver` (send init request)
- `portal merge/apiserver` (get init request)
- `portal merge/commission` (process commission request)
- `mars-apiserver` (receive portal mtz request, write keys into etcd/minio)
- all of the mars services, check what's wrong with `mars show mz harbor.system.marstb -S`

## Realization + Materialization Path

- `portal merge/model` (process model compliation)
- `portal merge/apiserver` (send realize request)
- `portal merge/realize` (process realize request)
- `portal merge/apiserver` (send mtz request)
- `portal merge/materialize` (process mtz request)
- all of the mars services, check what's wrong using either the WEBGUI or `mars show mz $MTZ -S`
- `mars-wireguard` / `portal merge/wgsvc` (for wireguard connections)
