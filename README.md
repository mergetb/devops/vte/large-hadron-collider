# What this is

This is instructions for setting up a Merge Development Environment.

By default, most things will be pulled via containers from Gitlab, so you don't have to build them locally.

# Prerequisites

```
sudo dnf install -y golang golang-bin python3 virt-install ansible httpd-tools podman gpgme-devel bcc bcc-devel device-mapper-devel wget kubernetes-client
```

# Initializing your portal and facility

```
# Init repos
./collider/init.sh

# Init portal appliance
# If you have a portal appliance already up, this will blow it up and recreate it from scratch
pushd appliance
./init-vanilla-k8s.sh

# Use ./init-vanilla-k8s.sh vanilla-k8s/portal-install-local.yml
# if you want to bootstrap from local files instead (instructions below on how to compile them)

popd

# Init basic facility files
pushd phobos
./generate-install-config.sh
popd

# Initialize users and the facility onto the portal
# In particular, check out register-users.sh so you can see how new users are created and the default passwords
collider/portal/register-users.sh
collider/portal/register-facility.sh

# Copy the SSH keys into the proper location
# Do this on both your account and the root account
./collider/facility/place-keys.sh
sudo ./collider/facility/place-keys.sh

# Create the phobos virtual facility
pushd phobos

## Do in this a separate terminal
sudo ./apiserver.sh

# Doing this as root causes less issues
sudo su

# You might have issues if your firewall and selinux are enabled.
# TODO: Firewall and Selinux configurations
# To disable in case you have issues:
# disable SELinux
sudo setenforce 0
sudo sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config

# disable Firewalld
sudo systemctl stop firewalld
sudo systemctl disable firewalld

# If you have phobos already up, this will blow it up and recreate it from scratch
./launch-phobos.sh

# Use ./launch-phobos.sh user-config-local.yml
# if you want to bootstrap from local files instead (instructions below on how to compile them)

exit

popd
```

# On Reboot

**WARNING NOT TESTED**

```
# Portal should be set to autostart, so you shouldn't have to relaunch it?
# But if you need to:
pushd appliance/vanilla-k8s
./relaunch-appliance.sh
popd

# Start up phobos???
sudo su
cd phobos
rvn deploy
```

# Updating your containers from CI

## The portal

TODO

## The facility

```
# Put ssh keys in correct place
# This will go into ~/.ssh as ~/.ssh/rvn and ~/.ssh/ops_rsa
# If you're already using these, change the names and edit phobos/phobos-ssh.config for the new names

./collider/facility/place-keys.sh

# Set mars config to use containers from the remote registry
# By default, you are set to the remote registry, so only have to do this if you've changed your configuration to the local registry
cd phobos
./generate-install-config.sh
scp -F phobos-ssh.config merge-install/mars-config.yml ops:~/.
ssh -F phobos-ssh.config ops mrs config init mars-config.yml -o
cd ..

# Bump all of the mars services on every machine
# If you want it to be smarter, comment out things in the script that you don't care about or don't use wildcards
./collider/facility/update-services-remote.sh

# Bump the harbor pod by deleting it and forcing mars-infrapod to recreate it
ssh -F phobos/phobos-ssh.config ifr "sudo podman pod rm -f harbor.system.marstb ; sudo systemctl restart mars-infrapod"

# You can also do this by demating and remating the harbor:
# ssh -F phobos/phobos-ssh.config ops "mrs deinit harbor ; sleep 10 ; mrs init harbor"
```

# Building and updating your own containers

## The portal

TODO

## The facility

```
# Create a local registry so you can pull
# You only have to do this once
sudo mkdir -p /var/lib/registry

sudo podman run \
    --privileged -d \
    --name registry \
    -p 5000:5000 \
    -v /var/lib/registry:/var/lib/registry \
    --restart=always registry:2

# Build the mars services
git clone https://gitlab.com/mergetb/facility/mars

## Do prereqs, only needs to be done once
cd mars
sudo -E ymk -j 1 tools.yml
cd ..

## Clean the build files, if you want to
cd mars
sudo ymk clean.yml
cd ..

# Build the services
./collider/facility/build-mars.sh

# Put ssh keys in correct place
# This will go into ~/.ssh as ~/.ssh/rvn and ~/.ssh/ops_rsa
# If you're already using these, change the names and edit phobos/phobos-ssh.config for the new names
./collider/facility/place-keys.sh

# Set mars config to use containers from the local registry instead
# You only need to do this if they're not already set to the local registry
cd phobos
sudo ./generate-install-config.sh user-config-local.yml
scp -F phobos-ssh.config merge-install/mars-config.yml ops:~/.
ssh -F phobos-ssh.config ops mrs config init mars-config.yml -o
cd ..

# Bump all of the mars services on every machine
# If you want it to be smarter, comment out things in the script that you don't care about or don't use wildcards
./collider/facility/update-services-local.sh

# Bump the harbor pod by deleting it and forcing mars-infrapod to recreate it
ssh -F phobos/phobos-ssh.config ifr "sudo podman pod rm -f harbor.system.marstb ; sudo systemctl restart mars-infrapod"

# You can also do this by demating and remating the harbor:
# ssh -F phobos/phobos-ssh.config ops "mrs deinit harbor ; sleep 10 ; mrs init harbor"
```

# Notes

## Getting the Webgui to work

The addresses are at:

- launch.mergetb.example.net
- api.mergetb.example.net
- auth.mergetb.example.net

You'll either need to manually an add exception for every address
OR
import the cert from `appliance/install/build/.cert/ca.pem` as a root CA.

Import instructions for Firefox:
https://support.globalsign.com/digital-certificates/digital-certificate-installation/install-client-digital-certificate-firefox-windows

## Getting the Webgui Jupiter Notebooks to Work

You need to have a wildcard DNS on \*.mergetb.example.net to point to either

- Your portal appliance
- The machine running the portal appliance
  - If you do this route, you'll also need haproxy installed and using the supplied config file, the instructions are below.
  - This has the side benefit of working while on the local network and not just on a devbox
- (You can also manually point all of the xdc addresses to the portal appliance too, but that's painful)

### Haproxy

This makes the portal appliance accessible from the outside of the machine, so you can connect to your Merge install as if it was a regular Merge testbed.

```
sudo dnf install -y haproxy
sudo cp collider/utils/haproxy.cfg /etc/config/haproxy.cfg
# if selinux is enabled
sudo setsebool -P haproxy_connect_any=1
sudo systemctl start haproxy
```
