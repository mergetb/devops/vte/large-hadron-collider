# Troubleshooting Tips

## Errors initing harbor

### First init fails with an rpc error

```
[rvn@ops ~]$ mrs init harbor
FATA[0000] portal commission: rpc error: code = Internal desc = failed to create init harbor request
```

In this case deinit, followed by another init often succeeds:

```
[rvn@ops ~]$ mrs deinit harbor
[rvn@ops ~]$ mrs init harbor
```

### mrs-install service fails on ifr

Sometimes during installation `mrs-install` service on `ifr` fails with the error:

`"read model: read minio object: The request signature we calculated does not match the signature you provided. Check your key and signing method."`

In that case try running `mrs init config` first (xxx shouldn't it always be first, before `mrs init minio`?).
It can also help with `mrs init minio` producing a similar error (xxx where? - in the logs somewhere).
