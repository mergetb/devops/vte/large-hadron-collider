#!/bin/bash

while [ ! -d "collider" ] && [ `pwd` != '/' ] ; do
  cd ..
done


git clone https://gitlab.com/mergetb/portal/appliance || true

pushd appliance
git checkout main
git submodule update --init
cd helm
git checkout main
git pull

popd

git clone https://gitlab.com/mergetb/devops/vte/phobos || true
cd phobos
git checkout ctran-dev

