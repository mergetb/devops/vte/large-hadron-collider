#!/bin/bash

while [ ! -d "phobos" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "phobos" ] ; then
    echo "can't find phobos directory"
    exit 1
fi

export GIT_SSL_NO_VERIFY=1
PHOBOS_DIR=phobos

set -ex

mrg login murphy -p mergetb123

mrg delete fac phobos || true
mrg delete pool default || true
mrg new pool default || true

mrg new fac phobos api.phobos.example.com $PHOBOS_DIR/model/cmd/phobos.xir $PHOBOS_DIR/merge-install/apiserver.pem

mrg pools add facility default phobos x0 x1 x2 x3 || true

mrg logout
