#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ] ; then
    echo "Usage: $0 <NAMESPACE> <PODNAME>"
    exit
fi

set -ex

kubectl get pods -n $1 | grep $2 | awk '{print $1}' | xargs kubectl delete pods -n $1
