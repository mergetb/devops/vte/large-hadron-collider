#!/bin/bash

export GIT_SSL_NO_VERIFY=1

set -ex

user=`kubectl -n merge describe job/ops-init | grep -i -A2 command: | tail -n 1 | tr -d "[:blank:]"`
pw=`kubectl -n merge describe job/ops-init | grep -i -A4 command: | tail -n 1 | tr -d "[:blank:]"`

mrg login olive -p mergetb
mrg demat links.hello.olive || true
mrg rel links.hello.olive || true
mrg delete exp hello.olive || true
mrg xdc detach links.olive || true
mrg del xdc links.olive || true
mrg delete experiment hello.olive || true

mrg login murphy -p mergetb
mrg delete fac phobos || true
mrg delete fac lighthouse || true

mrg logout
