#!/bin/bash

export GIT_SSL_NO_VERIFY=1

set -ex

mrg logout || true

mrg login olive -p mergetb

mrg xdc detach links.olive || true

mrg new xdc links.olive || true

sleep 2

mrg xdc attach links.olive links.hello.olive

sleep 2

mrg show vpn links.hello.olive
