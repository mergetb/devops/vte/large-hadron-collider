#!/bin/bash

export GIT_SSL_NO_VERIFY=1

set -ex

mrg logout || true

mrg login olive -p mergetb

mrg delete experiment hello.olive || true
mrg new experiment hello.olive 'My first experiment' || true

mrg whoami -t
sleep 10

git clone git@gitlab.com:christranq/hello-world.git || true
cd hello-world

git remote add mergetb https://git.mergetb.example.net/olive/hello || true

git checkout master
mrg whoami -t
until timeout 10s git push -u mergetb ; do sleep 2 ; done

sleep 5

mrg show experiment hello.olive --with-status

