#!/bin/bash

mrg login olive -p mergetb

until mrg show mat $1
do
    sleep 0.25
done

mrg show mat $1 -j > "$1".mat.json

