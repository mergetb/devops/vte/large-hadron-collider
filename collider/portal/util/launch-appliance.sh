#!/bin/bash

set -xe

VERSION=dev

echo Launching Merge Appliance version $VERSION

sudo virsh destroy sno-test || true

pushd pub

sudo virsh net-define net-$VERSION.xml
sudo virsh define vm-$VERSION.xml
sudo cp vm-$VERSION.qcow2 /var/lib/libvirt/images/sno-test.qcow2

sudo virsh net-start --network test-net || true
sudo virsh start sno-test

grep -q '^192.168.126.10' /etc/hosts || cat >> /etc/hosts <<EOF
192.168.126.10 api.test-cluster.redhat.com
192.168.126.10 console-openshift-console.apps.test-cluster.redhat.com
192.168.126.10 oauth-openshift.apps.test-cluster.redhat.com
192.168.126.10 default-route-openshift-image-registry.apps.test-cluster.redhat.com
192.168.126.10 mergetb.example.net
192.168.126.10 api.mergetb.example.net
192.168.126.10 git.mergetb.example.net
192.168.126.10 grpc.mergetb.example.net
192.168.126.10 auth.mergetb.example.net
192.168.126.10 launch.mergetb.example.net
192.168.126.10 jump.mergetb.example.io
EOF

export KUBECONFIG=`pwd`/kubeconfig-$VERSION

popd

set +x

until kubectl get deployments --all-namespaces=true; do
    echo '  waiting for VM to start up...'
    sleep 5
done

echo

until kubectl get deployments --all-namespaces=true | grep '0/1' > /dev/null; do
    echo 'waiting for deployments to reboot...'
    sleep 5
done

kubectl get deployments --all-namespaces=true

table=`kubectl get deployments --all-namespaces=true | awk 'NR!=1'`
namespaces=(`echo "$table" | awk '{print $1}'`)
deploys=(`echo "$table" | awk '{print $2}'`)

for i in "${!namespaces[@]}"; do
    kubectl rollout status -n "${namespaces[i]}" deployment/"${deploys[i]}"
done

kubectl get deployments --all-namespaces=true

kubectl get pods --all-namespaces=true

echo Appliance started up successfully!
