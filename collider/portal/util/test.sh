#!/bin/bash

set -ex

mrg login olive -p mergetb

# 4-node
NAME=links.hello.olive
#HASH=ecc59aec573270be4acc13c43b200796861d6bc3

#HASH=5e45902bc14970159efd9a04958674da929786b2
#HASH=787e443bf452c762906d905beff716dc8848f9a0
HASH=19a908c0a91494da9794bcd3721cb147606b54bd
#HASH=11df367ab56391d2c95c4923ed6cba8383d34c63

# 32-node
#HASH=67398f74c7422a08e18cbcaa95328fd814a6ae04

mrg demat $NAME || true
mrg rel $NAME || true
mrg xdc detach links.olive || true

sleep 2

mrg realize $NAME revision $HASH

time ./show-real.sh $NAME

mrg mat $NAME

time ./show-mat.sh $NAME

