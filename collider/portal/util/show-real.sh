#!/bin/bash

mrg login olive mergetb

until mrg show real $1
do
    sleep 0.25
done

mrg show real $1 -j > "$1".rlz.json
