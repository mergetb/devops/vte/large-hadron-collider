#!/bin/bash

export GIT_SSL_NO_VERIFY=1

while [ ! -d "appliance" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "appliance" ] ; then
    echo "can't find appliance directory"
    exit 1
fi

set -x

# mystery restart?
kubectl -n merge-xdc rollout restart deployment ssh-jump

user=`kubectl -n merge describe job/ops-init | grep -i -A2 command: | tail -n 1 | tr -d "[:blank:]"`
pw=`kubectl -n merge describe job/ops-init | grep -i -A4 command: | tail -n 1 | tr -d "[:blank:]"`

mrg logout || true

mrg config set server grpc.mergetb.example.net
mrg config set port 443

mrg login $user -p $pw --nokeys
mrg new portal entity-type Research Architecture Botnets Class Comprehensive Congestion DDoS DNS Evaluation Forensics Infrastructure "Insider Threat" Intrusions Malware Metrics Monitoring Multicast Other Overlays Privacy Routing Scanning Spam Spoofing Testbeds Traceback Trust Watermarking Wireless Worms || true

mrg new portal user-categories "Graduate student" Professor Researcher/Scientist "Teaching Assistant" "Undergraduate student" || true

mrg new portal institutions ISI MIT SFU "Greendale Community College" || true

mrg register olive olive@nowhere.com olive ISI Other 'United States' -p mergetb123 --usstate "California"
mrg register murphy murphy@nowhere.com murphy ISI Other 'United States' -p mergetb123 --usstate "California"
mrg register phobos phobos@nowhere.com phobos ISI Other 'United States' -p mergetb123 --usstate California

mrg activate user murphy
mrg update user admin murphy
mrg activate user olive
mrg activate user phobos

mrg logout

mrg login olive -p mergetb123

mrg logout
