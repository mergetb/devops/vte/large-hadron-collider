#!/bin/bash

while [ ! -d "appliance" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "appliance" ] ; then
    echo "can't find appliance directory"
    exit 1
fi

set -ex

pushd appliance
./init-vanilla-k8s.sh
popd

collider/portal/register-users.sh

sleep 60

collider/portal/register-facility.sh
