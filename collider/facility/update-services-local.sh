#!/bin/bash

while [ ! -d "mars" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "mars" ] ; then
    echo "can't find mars directory"
    exit 1
fi

if [ ! -d "phobos" ] ; then
    echo "can't find phobos directory"
    exit 1
fi

set -ex

export SSH_OPTIONS="-F phobos/phobos-ssh.config"

# feel free to comment out whatever you're not testing

# copy the mrs binary
echo copying binaries...

scp $SSH_OPTIONS mars/build/mrs rvn@ops:~/mrs &

scp $SSH_OPTIONS mars/build/mars-install ifr:~/mars-install &
scp $SSH_OPTIONS mars/build/mars-install x0:~/mars-install &
scp $SSH_OPTIONS mars/build/mars-install x1:~/mars-install &
scp $SSH_OPTIONS mars/build/mars-install x2:~/mars-install &
scp $SSH_OPTIONS mars/build/mars-install x3:~/mars-install &


# copy canopy to the switches
scp $SSH_OPTIONS mars/build/canopy infra:~/canopy &
scp $SSH_OPTIONS mars/build/canopy xp:~/canopy &

wait

# reboot the mars services on all of the nodes

echo rebooting services...

ssh $SSH_OPTIONS ops -- 'sudo cp ~/mrs /usr/bin/mrs' &

ssh $SSH_OPTIONS infra -- 'sudo systemctl stop canopy@mgmt.service && sudo cp ~/canopy /usr/local/bin/canopy && sudo systemctl start canopy@mgmt.service' &
ssh $SSH_OPTIONS xp -- 'sudo systemctl stop canopy@mgmt.service && sudo cp ~/canopy /usr/local/bin/canopy && sudo systemctl start canopy@mgmt.service' &


ssh $SSH_OPTIONS ifr -- 'sudo systemctl restart mars-canopy mars-apiserver mars-infrapod mars-wireguard' &

ssh $SSH_OPTIONS x0 -- 'sudo systemctl restart mars-mariner mars-canopy' &
ssh $SSH_OPTIONS x1 -- 'sudo systemctl restart mars-mariner mars-canopy' &
ssh $SSH_OPTIONS x2 -- 'sudo systemctl restart mars-mariner mars-canopy' &
ssh $SSH_OPTIONS x3 -- 'sudo systemctl restart mars-mariner mars-canopy' &

ssh $SSH_OPTIONS ifr -- 'sudo systemctl stop mars-install && sudo cp ~/mars-install /var/usrlocal/bin/mars-install && sudo systemctl start mars-install' &
ssh $SSH_OPTIONS x0 -- 'sudo systemctl stop mars-install && sudo cp ~/mars-install /usr/local/bin/mars-install && sudo systemctl start mars-install' &
ssh $SSH_OPTIONS x1 -- 'sudo systemctl stop mars-install && sudo cp ~/mars-install /usr/local/bin/mars-install && sudo systemctl start mars-install' &
ssh $SSH_OPTIONS x2 -- 'sudo systemctl stop mars-install && sudo cp ~/mars-install /usr/local/bin/mars-install && sudo systemctl start mars-install' &
ssh $SSH_OPTIONS x3 -- 'sudo systemctl stop mars-install && sudo cp ~/mars-install /usr/local/bin/mars-install && sudo systemctl start mars-install' &

wait

echo done!
