#!/bin/bash

while [ ! -d "phobos" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "phobos" ] ; then
    echo "can't find phobos directory"
    exit 1
fi


set -ex

mkdir -p -m700 ~/.ssh

sudo cp /var/rvn/ssh/rvn ~/.ssh/rvn
sudo cp phobos/merge-install/ops_rsa ~/.ssh/ops_rsa

sudo chown `id -u`:`id -g` ~/.ssh/rvn
sudo chown `id -u`:`id -g` ~/.ssh/ops_rsa

sudo chmod 0600 ~/.ssh/rvn
sudo chmod 0600 ~/.ssh/ops_rsa
