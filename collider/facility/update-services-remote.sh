#!/bin/bash

while [ ! -d "phobos" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "phobos" ] ; then
    echo "can't find phobos directory"
    exit 1
fi

set -ex

export SSH_OPTIONS="-F phobos/phobos-ssh.config"

# feel free to comment out whatever you're not testing

# fetch the mrs binary
echo fetching binaries...

ssh $SSH_OPTIONS ops -- 'sudo curl https://gitlab.com/mergetb/facility/mars/-/jobs/artifacts/main/raw/build/mrs?job=make -o /usr/bin/mrs' &

# copy canopy to the switches
ssh $SSH_OPTIONS infra 'sudo curl https://gitlab.com/mergetb/facility/mars/-/jobs/artifacts/main/raw/build/canopy?job=make -o ~/canopy' &
ssh $SSH_OPTIONS xp 'sudo curl https://gitlab.com/mergetb/facility/mars/-/jobs/artifacts/main/raw/build/canopy?job=make -o ~/canopy' &

wait

# reboot the mars services on all of the nodes

echo rebooting services...

ssh $SSH_OPTIONS infra -- 'sudo systemctl stop canopy@mgmt.service && sudo cp ~/canopy /usr/local/bin/canopy && sudo systemctl start canopy@mgmt.service' &
ssh $SSH_OPTIONS xp -- 'sudo systemctl stop canopy@mgmt.service && sudo cp ~/canopy /usr/local/bin/canopy && sudo systemctl start canopy@mgmt.service' &


ssh $SSH_OPTIONS ifr -- 'sudo systemctl restart mars-*' &

ssh $SSH_OPTIONS x0 -- 'sudo systemctl restart mars-*' &
ssh $SSH_OPTIONS x1 -- 'sudo systemctl restart mars-*' &
ssh $SSH_OPTIONS x2 -- 'sudo systemctl restart mars-*' &
ssh $SSH_OPTIONS x3 -- 'sudo systemctl restart mars-*' &

wait

echo done!
