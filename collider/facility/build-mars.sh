#!/bin/bash

export REGISTRY=localhost:5000
export REGISTRY_PATH=mergetb/facility/mars
export TAG=latest
export DOCKER_PUSH_ARGS=--tls-verify=false

while [ ! -d "mars" ] && [ `pwd` != '/' ] ; do
  cd ..
done

if [ ! -d "mars" ] ; then
    echo "can't find mars directory"
    exit 1
fi

set -ex

cd mars

#sudo -E ymk tools.go
#ymk clean.yml
ymk build.yml
ymk containers.yml
ymk push-containers.yml
